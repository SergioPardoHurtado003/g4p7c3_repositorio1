**Miembros integrantes del equipo de desarrollo**

- Reinaldo José Carrillo Roldán - PO [reijose1@gmail.com]
- name 2
- name 3
- name 4
- name 5
- name 6

---

## Aspectos Generales

En esta sección vamos a ingresar información directamente relacionada con la solución.  Favor tener en cuenta titular y subtitular de acuerdo 
con una estructura organizada
- Release 0.0.1 (Commit Inicial del Repositorio - actividades creadas hasta entrega de proyecto final en release 1.0.0)

---

## Otros Aspectos

En esta sección vamos a ingresar información adicional que deberá ser tenida en cuenta por el lector .  Favor tener en cuenta titular y subtitular 
de acuerdo con una estructura organizada

---

## Estandares Utilizados dentro del Proyecto

Vamos a Ingresar las versiones de cada una de las tecnologías utilizadas a lo largo de la construcción de la solución informática desarrollada.
- Python (3.9.4)

---
